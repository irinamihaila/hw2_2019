function addTokens(input, tokens) {
    if (typeof input !== "string") {
        throw new Error("Invalid input");

    }
    if (input.length < 6) {
        throw new Error("Input should have at least 6 characters");
    }

    if (tokens.filter(a => !(typeof a.tokenName === "string")).length) {
        throw new Error("Invalid array format");

    }
    if (input.search("...") !== -1) {

        for (let e of tokens) {
            let a="${"+e.tokenName+"}";
            input=input.replace("...",a);

        }

    }
    return input;





}

const app = {
    addTokens: addTokens
}

module.exports = app;
